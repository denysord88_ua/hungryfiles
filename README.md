# hungryfiles
https://stackoverflow.com/questions/20161602/loading-maven-dependencies-from-github
Now you can import a Java library from a GitHub repo using JitPack. In your pom.xml:

1. Add repository:
```
  <repositories>
    <repository>
      <id>jitpack.io</id>
      <url>https://jitpack.io</url>
    </repository>
  </repositories>
```
2. Add dependency
```
<dependency>
    <groupId>com.github.denysord88</groupId>
    <artifactId>hungryfiles</artifactId>
    <version>1.1</version>
</dependency>
```
It works because JitPack will check out the code and build it. So you'll end up downloading the jar.
If the project doesn't have a GitHub release then its possible to use a commit id as the version.
