package files.parser;

/**
 * @author denysord88
 * @author <a href='http://murrzik.su/'>http://murrzik.su/</a>
 * @version 1.0, 22 Oct 2016
 */
public interface FileParser {
    /**
     * Method gets text between boundaries.
     * If left boundary is empty string, it will get text from the begin of line.
     * If right boundary is empty string, it will get text to the end of line.
     *
     * @param lb  left boundary text
     * @param rb  right boundary text
     * @param src target text where to search
     * @return Text between boundaries or null if boundaries are not presented or one of parameters is null
     *
     * <br><br>Example:
     * <pre>{@code String example = "abcdefg";
     * String result = getTextByOutsideBoundaries("", "fg", example); // returns string "abcde"
     * result = getTextByBoundaries("abc", "", example); // returns string "defg"
     * result = getTextByBoundaries("bc", "f", example); // returns string "de"
     * result = getTextByBoundaries("12a", "fg", example); // returns null
     * }
     * </pre>
     */
    public static String getTextByOutsideBoundaries(String lb, String rb, String src) {
        if (null == lb || null == rb || null == src || src.isEmpty() || lb.length() + rb.length() > src.length())
            return null;
        int lbIndx = lb.isEmpty() ? 0 : src.indexOf(lb);
        if (!lb.isEmpty() && (lbIndx < 0 || src.length() < (lbIndx + lb.length() + rb.length()))) return null;
        int rbIndx = rb.isEmpty() ? src.length() : src.lastIndexOf(rb);
        if (!rb.isEmpty() && rbIndx < lbIndx + lb.length()) return null;
        return src.substring(lbIndx + lb.length(), rbIndx);
    }

    public static String getTextByNearestBoundaries(String lb, String rb, String src) {
        if (null == lb || null == rb || null == src || src.isEmpty() || lb.length() + rb.length() > src.length())
            return null;
        int lbIndx = lb.isEmpty() ? 0 : src.indexOf(lb);
        if (!lb.isEmpty() && (lbIndx < 0 || src.length() < (lbIndx + lb.length() + rb.length()))) return null;
        int rbIndx = rb.isEmpty() ? src.length() : src.indexOf(rb, lbIndx + lb.length());
        if (!rb.isEmpty() && rbIndx < lbIndx + lb.length()) return null;
        return src.substring(lbIndx + lb.length(), rbIndx);
    }
}
