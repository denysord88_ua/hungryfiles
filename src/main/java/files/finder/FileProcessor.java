package files.finder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;

/**
 * @author denysord88
 * @author <a href='http://murrzik.su/'>http://murrzik.su/</a>
 * @version 1.0, 22 Oct 2016
 */
public interface FileProcessor {
    /**
     * Method searching subfolders in the given folder's path
     *
     * @param dir path to the target folder
     * @return List of paths to all founded subfolders in target folder
     */
    public static LinkedList<Path> getFoldersList(Path dir) {
        LinkedList<Path> result = new LinkedList<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path entry : stream) {
                if (Files.isDirectory(entry)) result.add(entry);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static LinkedList<Path> getAllFoldersList(Path dir) {
        LinkedList<Path> result = new LinkedList<>();
        result.add(dir);
        LinkedList<Path> tmp = new LinkedList<>();
        LinkedList<Path> tmp2 = new LinkedList<>();
        tmp.add(dir);
        do {
            for (Path cDir : tmp) tmp2.addAll(getFoldersList(cDir));
            if (tmp2.isEmpty()) return result;
            tmp.clear();
            tmp.addAll(tmp2);
            result.addAll(tmp2);
            tmp2.clear();
        } while (!tmp.isEmpty());
        return result;
    }

    /**
     * Method searching files in the given folder path.
     * File names can be filtered by mask of the files name in parameter filesNameMask
     *
     * @param dir           path to the target folder
     * @param filesNameMask mask of the files name
     * @return List of paths to all founded files that matches to files name mask in the target folder
     */
    public static LinkedList<Path> getFilesList(Path dir, String filesNameMask) {
        LinkedList<Path> result = new LinkedList<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, filesNameMask)) {
            for (Path entry : stream) {
                result.add(entry);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static LinkedList<Path> getFilesListWithSubdir(Path dir, String filesNameMask) {
        LinkedList<Path> srcList = getAllFoldersList(dir);
        LinkedList<Path> result = new LinkedList<>();
        for (Path currentDir : srcList) {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(currentDir, filesNameMask)) {
                for (Path entry : stream) {
                    result.add(entry);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static void writeLinesToFile(LinkedList<String> lines, Path file, boolean overwrite) {
        try (PrintWriter out = new PrintWriter(new FileWriter(file.toFile(), !overwrite))) {
            for (String line : lines) out.println(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeLineToFile(String line, Path file, boolean overwrite) {
        try (PrintWriter out = new PrintWriter(new FileWriter(file.toFile(), !overwrite))) {
            out.println(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static LinkedList<String> readLinesFromFile(Path filePath) {
        LinkedList<String> lines = new LinkedList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath.toAbsolutePath().toString()))) {
            String line;
            while ((line = br.readLine()) != null) lines.add(line);
        } catch (IOException e) {
            e.printStackTrace();
            return lines;
        }
        return lines;
    }

    public static String readTextFromFile(Path filePath, String linesSeparator) {
        StringBuffer sb = new StringBuffer();
        LinkedList<String> lines = readLinesFromFile(filePath);
        for(String line: lines) sb.append(line + linesSeparator);
        return sb.toString();
    }
}